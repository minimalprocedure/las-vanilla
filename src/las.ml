(* Look-and-say - Conway sequence printer *)
(* MMG - zairik@gmail.com - Public Domain *)

module List = struct

  include List

  let init n ~f =
    let rec loop i acc =
      match i with
      | 0 -> acc
      | _ -> loop (i-1) (f (i-1) :: acc)
    in
    loop n []
  ;;

  let last ls =
    List.nth ls ((List.length ls) - 1)

end

let mk_list v l = List.init l ~f:(fun x -> v);;

let genlas n l =
  let rec las = function
    | [], t -> List.rev t
    | h :: hs, [] -> las (hs, [h; 1])
    | h :: hs, a :: b :: t when h = a -> las (hs, a :: 1 + b :: t)
    | h :: hs, t -> las (hs, h :: 1 :: t)
  in
  let ls = mk_list n l in
  let f acc e =
    let last = List.last acc in
    acc @ [las(last, [])]
  in
  List.fold_left f (mk_list [n] 1) ls
;;

let print ls =
  let str = String.concat "" (List.map (string_of_int) ls) in
  Printf.printf "%s\n" str
;;

let limit = ref 0
let digit = ref 1

let args_parse =
  let opts = [
    ("-d", Arg.Set_int digit, ": Digit number 0..9");
    ("-l", Arg.Set_int limit, ": Recursion limit");
  ] in
  let usage = "las sequence calculator\nusage: " ^ Sys.argv.(0) ^ " [-d 0..9]" ^ " [-l number]" in
  Arg.parse opts (fun x -> raise (Arg.Bad ("Bad argument : " ^ x))) usage
;;

let () =
  args_parse;
  let digit = !digit in
  let limit = !limit in
  let result = genlas digit limit in
  List.iter print result;
;;
